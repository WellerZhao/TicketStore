package com.ticketstore.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.ticketstore.domain.Ticket;
import com.ticketstore.service.ITicketService;

@Controller("ticketAction")
@Scope("prototype")
public class TicketAction {
	@Resource
	private ITicketService ticketService = null;
	private List<Ticket> ticketList;
	
	public String list(){
		ticketList = ticketService.listAllTicket();
		return "list";
	}

	public List<Ticket> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}
}
