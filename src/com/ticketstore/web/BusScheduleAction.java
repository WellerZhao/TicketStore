package com.ticketstore.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;
import com.ticketstore.service.IBusScheduleService;

@Controller("busScheduleAction")
@Scope("prototype")
public class BusScheduleAction {
	@Resource
	private IBusScheduleService busScheduleService = null;
	private List<BusSchedule> busScheduleList;
	private Integer busScheduleID;
	private BusSchedule busSchedule;
	private String fromLocation;
	private String toLocation;
	private Date minTakeOffTime;
	private Date maxTakeOffTime;
	
	public String search(){
		String whereHQL="b.ticketLeftNum > 0 and b.takeOffTime > ?";
		List<Object> params = new ArrayList<Object>();
		if(!fromLocation.trim().equals("")){
			whereHQL += " and ";
			whereHQL += "b.fromLocation like ?";
			System.out.println("出发地："+fromLocation);
			params.add("%"+fromLocation+"%");
		}
		if(!toLocation.trim().equals("")){
			if(!whereHQL.trim().equals("")){
				whereHQL += " and ";
			}
			whereHQL += "b.toLocation like ?";
			System.out.println("目的地："+toLocation);
			params.add("%"+toLocation+"%");
		}
		if(minTakeOffTime != null){
			if(!whereHQL.trim().equals("")){
				whereHQL += " and ";
			}
			whereHQL += "b.takeOffTime > ?";
			System.out.println("minTakeOffTime:"+minTakeOffTime);
			params.add(minTakeOffTime);
		}
		if(maxTakeOffTime != null){
			if(!whereHQL.trim().equals("")){
				whereHQL += " and ";
			}
			whereHQL += "b.takeOffTime < ?";
			System.out.println("maxTakeOffTime:"+maxTakeOffTime);
			params.add(maxTakeOffTime);
		}
		System.out.println("whereHQL:"+whereHQL);
		busScheduleList = busScheduleService.searchBusSchedule(whereHQL, params);
		return "listForUser";
	}
	
	public String findBusSchedule(){
		System.out.println("预订的车程班次ID："+busScheduleID);
		busSchedule = busScheduleService.findBusSchedule(busScheduleID);
		return "orderTicket";
	}
	
	public String listAllBusSchedule(){
		busScheduleList = busScheduleService.listAllBusSchedule();
		return "listForUser";
	}
	
	public String listBusScheduleForContractor(){
		Contractor contractor = (Contractor) ActionContext.getContext().getSession().get("LoginedContractor");
		busScheduleList = busScheduleService.listBusScheduleForContractor(contractor);
		return "listForContractor";
	}
	
	public String deleteBusSchedule(){
		Contractor contractor = (Contractor) ActionContext.getContext().getSession().get("LoginedContractor");
		System.out.println("要删除的班次ID："+busScheduleID);
		if(busScheduleService.deleteBusSchedule(busScheduleID)){
			busScheduleList = busScheduleService.listBusScheduleForContractor(contractor);
			return "listForContractor";
		}
		return "listForContractor";
	}
	
	public String addBusSchedule(){
		Contractor contractor = (Contractor) ActionContext.getContext().getSession().get("LoginedContractor");
		System.out.println("承包商ID："+contractor.getContractorID());
		busSchedule.setContractor(contractor);
		if(busScheduleService.addBusSchedule(busSchedule)){
			busScheduleList = busScheduleService.listBusScheduleForContractor(contractor);
			return "listForContractor";
		}
		return "listForContractor";
	}

	public List<BusSchedule> getBusScheduleList() {
		return busScheduleList;
	}

	public void setBusScheduleList(List<BusSchedule> busScheduleList) {
		this.busScheduleList = busScheduleList;
	}

	public Integer getBusScheduleID() {
		return busScheduleID;
	}

	public void setBusScheduleID(Integer busScheduleID) {
		this.busScheduleID = busScheduleID;
	}

	public BusSchedule getBusSchedule() {
		return busSchedule;
	}

	public void setBusSchedule(BusSchedule busSchedule) {
		this.busSchedule = busSchedule;
	}
	
	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}

	public Date getMinTakeOffTime() {
		return minTakeOffTime;
	}

	public void setMinTakeOffTime(Date minTakeOffTime) {
		this.minTakeOffTime = minTakeOffTime;
	}

	public Date getMaxTakeOffTime() {
		return maxTakeOffTime;
	}

	public void setMaxTakeOffTime(Date maxTakeOffTime) {
		this.maxTakeOffTime = maxTakeOffTime;
	}

}
