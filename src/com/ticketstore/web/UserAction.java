package com.ticketstore.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.ticketstore.domain.User;
import com.ticketstore.service.IUserService;

@Controller("userAction")
@Scope("prototype")
public class UserAction{
	
	private String confirmPassword;
	@Resource
	private IUserService userService = null;
	private List<User> userList;
	private User user;
	private Integer userID;
	
	public String login(){
		Integer userID = userService.login(user);
		if(userID != null){
			System.out.println("Login Success!");
			user.setUserID(userID);
			ActionContext.getContext().getSession().put("LoginedUser", user);
			return "success";
		}else{
			return "input";
		}
	}
	
	public String register(){
		if(confirmPassword.equals(user.getPassword())){
			if(userService.register(user)){
				System.out.println(user.getUserID());
				return "success";
			}
		}
		return "registerFailed";
	}
	
	public String listAllUser(){
		userList = userService.listAllUser();
		return "list";
	}
	
	public String logout(){
		ActionContext.getContext().getSession().remove("LoginedUser");
		return "input";
	}
	public String deleteUser(){
		System.out.println("要删除的ID："+userID);
		if(userService.deleteUser(userID)){
			listAllUser();
			return "list";
		}
		return "list";
	}
	
	public String addUser(){
		if(userService.addUser(user)){
			return "list";
		}
			return "list";
		
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	
	
}
