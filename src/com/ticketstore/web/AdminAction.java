package com.ticketstore.web;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.ticketstore.domain.Admin;
import com.ticketstore.service.IAdminService;

@Controller("adminAction")
@Scope("prototype")
public class AdminAction {
	private Admin admin;
	@Resource
	private IAdminService adminService = null;
	
	public String login(){
		if(adminService.login(admin)){
			System.out.println("Admin Login Success!!!");
			ActionContext.getContext().getSession().put("LoginedAdmin", admin);
			return "success";
		}
		return "input";
	}
	
	public String logout(){
		ActionContext.getContext().getSession().remove("LoginedAdmin");
		return "input";
	}
	
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

}
