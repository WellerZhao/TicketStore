package com.ticketstore.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;
import com.ticketstore.domain.Order;
import com.ticketstore.domain.User;
import com.ticketstore.service.IBusScheduleService;
import com.ticketstore.service.IContractorService;
import com.ticketstore.service.IOrderService;

@Controller("orderAction")
@Scope("prototype")
public class OrderAction {
	@Resource
	private IOrderService orderService = null;
	@Resource
	private IBusScheduleService busScheduleService = null;
	@Resource
	private IContractorService contractorService = null;
	
	private List<Order> orderList;
	private Order order;
	private Integer busScheduleID;
	
	public String orderTicket(){
		User user = (User) ActionContext.getContext().getSession().get("LoginedUser");
		if(user == null) {
			return "login";
		}
		BusSchedule busSchedule = busScheduleService.findBusSchedule(busScheduleID);
		order.setBusSchedule(busSchedule);
		order.setUser(user);
		order.setPay(false);
		if(orderService.orderTicket(order)){
			System.out.println("插入订单成功");
			busSchedule.setTicketLeftNum(busSchedule.getTicketLeftNum()-order.getOrderTicketNum());
			if(busScheduleService.updateBusSchedule(busSchedule)){
				System.out.println("更新班次信息成功");
			}
			Contractor contractor = busSchedule.getContractor();
			contractor.setTotalProfit(contractor.getTotalProfit()+order.getTotalPrice());
			if(contractorService.updateContractor(contractor)){
				System.out.println("更新承包商信息成功");
			}
		}
		return "orderSuccess";
	}
	
	public String listOrderForUser(){
		User user = (User) ActionContext.getContext().getSession().get("LoginedUser");
		if(user == null) {
			return "login";
		}
		orderList = orderService.listOrderForUser(user.getUserID());
		return "listForUser";
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Integer getBusScheduleID() {
		return busScheduleID;
	}

	public void setBusScheduleID(Integer busScheduleID) {
		this.busScheduleID = busScheduleID;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	
}
