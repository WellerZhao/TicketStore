package com.ticketstore.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.ticketstore.domain.Contractor;
import com.ticketstore.service.IContractorService;

@Controller("contractorAction")
@Scope("prototype")
public class ContractorAction {
	@Resource
	private IContractorService contractorService = null;
	private List<Contractor> contractorList;
	private Contractor contractor;
	private Integer contractorID;
	
	public String listAllContractor(){
		contractorList = contractorService.listAllContractor();
		return "list";
	}

	public String login(){
		Integer contractorID = contractorService.login(contractor);
		if(contractorID != null){
			contractor.setContractorID(contractorID);
			System.out.println("Contractor Login Success!!!");
			ActionContext.getContext().getSession().put("LoginedContractor", contractor);
			return "success";
		}
		return "input";
	}
	
	public String logout(){
		ActionContext.getContext().getSession().remove("LoginedContractor");
		return "input";
	}
	
	public String deleteContractor(){
		System.out.println("要删除的ID："+contractorID);
		if(contractorService.deleteContractor(contractorID)){
			listAllContractor();
			return "list";
		}
		return "list";
	}
	
	public String addContractor(){
		if(contractorService.addContractor(contractor)){
			return "list";
		}
		return "list";
	}
	
	public String showContractor(){
		contractor = (Contractor) ActionContext.getContext().getSession().get("LoginedContractor");
		contractor = contractorService.findContractor(contractor.getContractorID());
		return "showContractor";
	}
	
	public Integer getContractorID() {
		return contractorID;
	}

	public void setContractorID(Integer contractorID) {
		this.contractorID = contractorID;
	}

	public List<Contractor> getContractorList() {
		return contractorList;
	}

	public void setContractorList(List<Contractor> contractorList) {
		this.contractorList = contractorList;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}
	
}
