package com.ticketstore.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orderlist")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer orderID;
	@OneToOne(cascade = {CascadeType.REMOVE})
	@JoinColumn(name = "busScheduleID")
	private BusSchedule busSchedule;
	//票的数量
	private int orderTicketNum;
	//订单总价
	private int totalPrice;
	//订单生成日期
	private Date orderDate;
	//所属用户
	@ManyToOne(cascade = {CascadeType.REMOVE})
	@JoinColumn(name = "userID")
	private User user;
	//一个订单只能买一种车程票，但是车票可以买多张
//	@OneToOne
//	@JoinColumn(name = "ticketID")
//	private Ticket ticket;
	//订单状态，是否已付款
	private boolean isPay;
	
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public BusSchedule getBusSchedule() {
		return busSchedule;
	}
	public void setBusSchedule(BusSchedule busSchedule) {
		this.busSchedule = busSchedule;
	}
	public int getOrderTicketNum() {
		return orderTicketNum;
	}
	public void setOrderTicketNum(int orderTicketNum) {
		this.orderTicketNum = orderTicketNum;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
//	public Ticket getTicket() {
//		return ticket;
//	}
//	public void setTicket(Ticket ticket) {
//		this.ticket = ticket;
//	}
	public boolean isPay() {
		return isPay;
	}
	public void setPay(boolean isPay) {
		this.isPay = isPay;
	}
}
