package com.ticketstore.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ticketID;
	//一张车票只会有一程，但是一班车程会有多张票，车票与车程半次为多对一
	@ManyToOne
	@JoinColumn(name = "busScheduleID")
	private BusSchedule busSchedule;
	@ManyToOne
	@JoinColumn(name = "userID")
	private User user;
	
	public Integer getTicketID() {
		return ticketID;
	}
	public void setTicketID(Integer ticketID) {
		this.ticketID = ticketID;
	}
	public BusSchedule getBusSchedule() {
		return busSchedule;
	}
	public void setBusSchedule(BusSchedule busSchedule) {
		this.busSchedule = busSchedule;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
