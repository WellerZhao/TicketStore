package com.ticketstore.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contractor")
public class Contractor {
	/*
	 * 包车承包商
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer contractorID;
	private String userName;
	private String password;
	private String realName;
	private String tradeMark;
	private String telephoneNum;
	//承包商的总共收益
	private int totalProfit;
	
	public Integer getContractorID() {
		return contractorID;
	}
	public void setContractorID(Integer contractorID) {
		this.contractorID = contractorID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getTelephoneNum() {
		return telephoneNum;
	}
	public void setTelephoneNum(String telephoneNum) {
		this.telephoneNum = telephoneNum;
	}
	public String getTradeMark() {
		return tradeMark;
	}
	public void setTradeMark(String tradeMark) {
		this.tradeMark = tradeMark;
	}
	public int getTotalProfit() {
		return totalProfit;
	}
	public void setTotalProfit(int totalProfit) {
		this.totalProfit = totalProfit;
	}
	
}
