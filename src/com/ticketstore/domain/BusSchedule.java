package com.ticketstore.domain;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bus_schedule")
public class BusSchedule {
	/*
	 * 车程半次
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer busScheduleID;
	//出发地
	private String fromLocation;
	//目的地
	private String toLocation;
	//发车时间
	private Timestamp takeOffTime;
	//票价
	private int priceForASeat;
	//一班车总共票数
	private int ticketNum;
	//剩余票数
	private int ticketLeftNum;
	//所属承包商
	@ManyToOne(cascade = {javax.persistence.CascadeType.REMOVE})
	@JoinColumn(name = "contractorID")
	private Contractor contractor;
	
	public Integer getBusScheduleID() {
		return busScheduleID;
	}
	public void setBusScheduleID(Integer busScheduleID) {
		this.busScheduleID = busScheduleID;
	}
	public String getFromLocation() {
		return fromLocation;
	}
	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}
	public String getToLocation() {
		return toLocation;
	}
	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}
	public Timestamp getTakeOffTime() {
		return takeOffTime;
	}
	public void setTakeOffTime(Timestamp takeOffTime) {
		this.takeOffTime = takeOffTime;
	}
	public int getPriceForASeat() {
		return priceForASeat;
	}
	public void setPriceForASeat(int priceForASeat) {
		this.priceForASeat = priceForASeat;
	}
	public int getTicketNum() {
		return ticketNum;
	}
	public void setTicketNum(int ticketNum) {
		this.ticketNum = ticketNum;
	}
	public int getTicketLeftNum() {
		return ticketLeftNum;
	}
	public void setTicketLeftNum(int ticketLeftNum) {
		this.ticketLeftNum = ticketLeftNum;
	}
	public Contractor getContractor() {
		return contractor;
	}
	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}
}
