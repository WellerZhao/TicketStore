package com.ticketstore.service;

import java.util.List;

import com.ticketstore.domain.User;

public interface IUserService {
	public Integer login(User user);
	public boolean register(User user);
	public List<User> listAllUser();
	public boolean deleteUser(Integer userID);
	public boolean addUser(User user);
}
