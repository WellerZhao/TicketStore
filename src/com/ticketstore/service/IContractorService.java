package com.ticketstore.service;

import java.util.List;

import com.ticketstore.domain.Contractor;

public interface IContractorService {
	public List<Contractor> listAllContractor();
	public Integer login(Contractor contractor);
	public boolean deleteContractor(Integer contractorID);
	public boolean addContractor(Contractor contractor);
	public Contractor findContractor(Integer contractorID);
	public boolean updateContractor(Contractor contractor);
}
