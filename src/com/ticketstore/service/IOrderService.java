package com.ticketstore.service;

import java.util.List;

import com.ticketstore.domain.Order;

public interface IOrderService {
	public boolean orderTicket(Order order);
	public List<Order> listOrderForUser(Integer userID);
}
