package com.ticketstore.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.IUserDao;
import com.ticketstore.domain.User;
import com.ticketstore.service.IUserService;

@Service("userService")
public class UserService implements IUserService {
	private UserService userService = null;
	@Resource
	private IUserDao userDao = null;
	
	private UserService(){
		
	}
	
	public UserService getInstance(){
		if(userService == null){
			userService = new UserService();
		}
		return userService;
	}

	@Override
	public Integer login(User user) {
		return userDao.login(user);
	}

	@Override
	public boolean register(User user) {
		return userDao.register(user);
	}

	@Override
	public List<User> listAllUser() {
		return userDao.list();
	}

	@Override
	public boolean deleteUser(Integer userID) {
		return userDao.deleteUser(userID);
	}

	@Override
	public boolean addUser(User user) {
		return userDao.addUser(user);
	}
	
}
