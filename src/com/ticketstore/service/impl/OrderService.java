package com.ticketstore.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.IOrderDao;
import com.ticketstore.domain.Order;
import com.ticketstore.service.IOrderService;

@Service("orderService")
public class OrderService implements IOrderService{
	private OrderService orderService = null;
	@Resource
	private IOrderDao orderDao = null;
	private OrderService(){
		
	}
	
	public OrderService getInstance(){
		if(orderService == null){
			orderService = new OrderService();
		}
		return orderService;
	}

	@Override
	public boolean orderTicket(Order order) {
		return orderDao.insertOrder(order);
	}

	@Override
	public List<Order> listOrderForUser(Integer userID) {
		return orderDao.listOrderForUser(userID);
	}
	
}
