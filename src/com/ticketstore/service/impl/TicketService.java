package com.ticketstore.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.ITicketDao;
import com.ticketstore.domain.Ticket;
import com.ticketstore.service.ITicketService;

@Service("ticketService")
public class TicketService implements ITicketService {
	private TicketService ticketService = null;
	@Resource
	private ITicketDao ticketDao = null;
	private TicketService(){
		
	}
	
	public TicketService getInstance(){
		if(ticketService == null){
			ticketService = new TicketService();
		}
		return ticketService;
	}

	@Override
	public List<Ticket> listAllTicket() {
		return ticketDao.listAllTicket();
	}
	
}
