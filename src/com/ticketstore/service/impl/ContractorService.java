package com.ticketstore.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.IContractorDao;
import com.ticketstore.domain.Contractor;
import com.ticketstore.service.IContractorService;

@Service("contractorService")
public class ContractorService implements IContractorService {
	private ContractorService contractorService = null;
	@Resource
	private IContractorDao contractorDao = null;
	
	private ContractorService(){
		
	}
	
	public ContractorService getInstance(){
		if(contractorService == null){
			contractorService = new ContractorService();
		}
		return contractorService;
	}

	@Override
	public List<Contractor> listAllContractor() {
		return contractorDao.list();
	}

	@Override
	public Integer login(Contractor contractor) {
		return contractorDao.login(contractor);
	}

	@Override
	public boolean deleteContractor(Integer contractorID) {
		return contractorDao.deleteContractor(contractorID);
	}

	@Override
	public boolean addContractor(Contractor contractor) {
		return contractorDao.addContractor(contractor);
	}

	@Override
	public Contractor findContractor(Integer contractorID) {
		return contractorDao.findContractor(contractorID);
	}

	@Override
	public boolean updateContractor(Contractor contractor) {
		return contractorDao.updateContractor(contractor);
	}
	
}
