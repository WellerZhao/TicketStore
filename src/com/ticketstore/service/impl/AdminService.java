package com.ticketstore.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.IAdminDao;
import com.ticketstore.domain.Admin;
import com.ticketstore.service.IAdminService;

@Service("adminService")
public class AdminService implements IAdminService{
	private AdminService adminService = null;
	@Resource
	private IAdminDao adminDao;
	
	private AdminService(){
		
	}
	
	public AdminService getInstance(){
		if(adminService == null){
			adminService = new AdminService();
		}
		return adminService;
	}

	@Override
	public boolean login(Admin admin) {
		return adminDao.login(admin);
	}

	
}
