package com.ticketstore.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ticketstore.dao.IBusScheduleDao;
import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;
import com.ticketstore.service.IBusScheduleService;

@Service("busScheduleService")
public class BusScheduleService implements IBusScheduleService {
	private BusScheduleService busScheduleService = null;
	@Resource
	private IBusScheduleDao busScheduleDao = null;
	
	private BusScheduleService(){
		
	}
	
	public BusScheduleService getInstance(){
		if(busScheduleService == null)
		{
			busScheduleService = new BusScheduleService();
		}
		return busScheduleService;
	}

	@Override
	public List<BusSchedule> listAllBusSchedule() {
		return busScheduleDao.list();
	}

	@Override
	public List<BusSchedule> listBusScheduleForContractor(Contractor contractor) {
		return busScheduleDao.listForContractor(contractor);
	}

	@Override
	public boolean deleteBusSchedule(Integer busScheduleID) {
		return busScheduleDao.deleteBusSchedule(busScheduleID);
	}

	@Override
	public boolean addBusSchedule(BusSchedule busSchedule) {
		return busScheduleDao.addBusSchedule(busSchedule);
	}

	@Override
	public List<BusSchedule> searchBusSchedule(String whereHQL, List<Object> params) {
		return busScheduleDao.searchBusSchedule(whereHQL, params);
	}

	@Override
	public BusSchedule findBusSchedule(Integer busScheduleID) {
		return busScheduleDao.findBusSchedule(busScheduleID);
	}

	@Override
	public boolean updateBusSchedule(BusSchedule busSchedule) {
		return busScheduleDao.updateBusSchedule(busSchedule);
	}
	
	
	
}
