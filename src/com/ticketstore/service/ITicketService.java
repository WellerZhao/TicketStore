package com.ticketstore.service;

import java.util.List;

import com.ticketstore.domain.Ticket;

public interface ITicketService {
	public List<Ticket> listAllTicket();
}
