package com.ticketstore.service;

import java.util.List;

import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;

public interface IBusScheduleService {
	public List<BusSchedule> listAllBusSchedule();
	public List<BusSchedule> listBusScheduleForContractor(Contractor contractor);
	public boolean deleteBusSchedule(Integer busScheduleID);
	public boolean addBusSchedule(BusSchedule busSchedule);
	public List<BusSchedule> searchBusSchedule(String whereHQL, List<Object> params);
	public BusSchedule findBusSchedule(Integer busScheduleID);
	public boolean updateBusSchedule(BusSchedule busSchedule);
}
