package com.ticketstore.service;

import com.ticketstore.domain.Admin;

public interface IAdminService {
	public boolean login(Admin admin);
}
