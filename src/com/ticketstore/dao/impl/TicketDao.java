package com.ticketstore.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.ticketstore.dao.ITicketDao;
import com.ticketstore.domain.Ticket;

@Repository("ticketDao")
public class TicketDao implements ITicketDao{
	private TicketDao ticketDao = null;
	@Resource
	private SessionFactory sessionFactory = null;
	private TicketDao(){
		
	}
	public TicketDao getInstance(){
		if(ticketDao == null){
			ticketDao = new TicketDao();
		}
		return ticketDao;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Ticket> listAllTicket(){
		List<Ticket> ticketList = new ArrayList<Ticket>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			ticketList = session.createQuery("from Ticket").list();
			transaction.commit();
			return ticketList;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}
}
