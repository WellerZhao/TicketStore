package com.ticketstore.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.ticketstore.dao.IContractorDao;
import com.ticketstore.domain.Contractor;

@Repository("contractorDao")
public class ContractorDao implements IContractorDao {
	private ContractorDao contractorDao = null;
	@Resource
	private SessionFactory sessionFactory = null;
	private ContractorDao(){
		
	}
	public ContractorDao getInstance(){
		if(contractorDao == null){
			contractorDao = new ContractorDao();
		}
		return contractorDao;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Contractor> list() {
		List<Contractor> contractorList = new ArrayList<Contractor>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			contractorList = (List<Contractor>)session.createQuery("from Contractor").list();
			transaction.commit();
			return contractorList;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Integer login(Contractor contractor) {
		Session session = null;
		Transaction transaction = null;
		Integer contractorID = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			if(!session.createQuery("from Contractor as c where c.userName=:cName").setParameter("cName", contractor.getUserName()).list().isEmpty())
			{
				if(!session.createQuery("from Contractor as c where c.userName=:cName and c.password=:cPwd")
						.setParameter("cName", contractor.getUserName()).setParameter("cPwd", contractor.getPassword()).list().isEmpty()){
					contractorID = (Integer) session.createQuery("select contractorID from Contractor as c where c.userName=:cName")
							.setParameter("cName", contractor.getUserName()).list().get(0);
					System.out.println(contractorID);
					transaction.commit();
					return contractorID;
				}
				
			}
			transaction.commit();
			return null;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public boolean deleteContractor(Integer contractorID) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			Contractor contractor = (Contractor) session.get(Contractor.class, contractorID);
			session.delete(contractor);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
	@Override
	public boolean addContractor(Contractor contractor) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.save(contractor);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
	@Override
	public Contractor findContractor(Integer contractorID) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			Contractor contractor = (Contractor) session.get(Contractor.class, contractorID);
			transaction.commit();
			return contractor;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean updateContractor(Contractor contractor) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.update(contractor);
			transaction.commit();
			return true;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
	
	
	
}
