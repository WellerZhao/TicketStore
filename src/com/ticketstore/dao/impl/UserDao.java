package com.ticketstore.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.ticketstore.dao.IUserDao;
import com.ticketstore.domain.User;

@Repository("userDao")
public class UserDao implements IUserDao {
	private UserDao userDao = null;
	@Resource
	private SessionFactory sessionFactory;
	
	private UserDao(){
		
	}
	
	public UserDao getInstance(){
		if(userDao == null){
			userDao = new UserDao();
		}
		return userDao;
	}

	@Override
	public Integer login(User user) {
		Session session = null;
		Transaction transaction = null;
		Integer userID = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			if(!session.createQuery("from User as u where u.userName=:uName").setParameter("uName", user.getUserName()).list().isEmpty())
			{
				if(!session.createQuery("from User as u where u.userName=:uName and u.password=:uPwd")
						.setParameter("uName", user.getUserName()).setParameter("uPwd", user.getPassword()).list().isEmpty()){
					userID =  (Integer) session.createQuery("select userID from User as u where u.userName=:uName")
							.setParameter("uName", user.getUserName()).list().get(0);
					transaction.commit();
					return userID;
				}
				
			}
			transaction.commit();
			return null;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
			
		}
	}

	@Override
	public boolean register(User user) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.save(user);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> list() {
		List<User> userList = new ArrayList<User>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			userList = (List<User>)session.createQuery("from User").list();
			transaction.commit();
			return userList;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean deleteUser(Integer userID) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			User user = (User) session.get(User.class, userID);
			session.delete(user);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public boolean addUser(User user) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.save(user);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	
	
}
