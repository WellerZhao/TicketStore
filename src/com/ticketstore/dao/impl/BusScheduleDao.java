package com.ticketstore.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.ticketstore.dao.IBusScheduleDao;
import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;
@Repository("busScheduleDao")
public class BusScheduleDao implements IBusScheduleDao{
	private BusScheduleDao busScheduleDao = null;
	@Resource
	private SessionFactory sessionFactory = null;
	
	private BusScheduleDao(){
		
	}
	
	public BusScheduleDao getInstance(){
		if(busScheduleDao == null){
			busScheduleDao = new BusScheduleDao();
		}
		return busScheduleDao;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusSchedule> list() {
		List<BusSchedule> busScheduleList = new ArrayList<BusSchedule>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			busScheduleList = (List<BusSchedule>)session.createQuery("from BusSchedule where takeOffTime > ? and ticketLeftNum > 0")
					.setParameter(0, new Date()).list();
			transaction.commit();
			return busScheduleList;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusSchedule> listForContractor(Contractor contractor) {
		List<BusSchedule> busScheduleListForContractor = new ArrayList<BusSchedule>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			busScheduleListForContractor = (List<BusSchedule>)session.createQuery("from BusSchedule as b where b.contractor.contractorID=:cID")
					.setParameter("cID", contractor.getContractorID()).list();
			transaction.commit();
			return busScheduleListForContractor;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean deleteBusSchedule(Integer busScheduleID) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			BusSchedule busSchedule = (BusSchedule) session.get(BusSchedule.class, busScheduleID);
			session.delete(busSchedule);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean addBusSchedule(BusSchedule busSchedule) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.save(busSchedule);
			transaction.commit();
			return true;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusSchedule> searchBusSchedule(String whereHQL, List<Object> params) {
		List<BusSchedule> busScheduleListForSearch = new ArrayList<BusSchedule>();
		String hql = "from BusSchedule as b " + (whereHQL.trim().equals("")?"":"where " + whereHQL);
		Session session = null;
		Transaction transaction = null;
		try{
			System.out.println("HQL:"+hql);
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			
			
			Query query = session.createQuery(hql).setParameter(0, new Date());
			
			if(!whereHQL.trim().equals("")){
				for(int i = 1;i<=params.size();i++){
					query.setParameter(i, params.get(i-1));
					System.out.println("第"+i+"个参数"+params.get(i-1));
				}
			}
			
			busScheduleListForSearch = (List<BusSchedule>)query.list();
			System.out.println("输出列表："+busScheduleListForSearch);
			transaction.commit();
			return busScheduleListForSearch;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BusSchedule findBusSchedule(Integer busScheduleID) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			BusSchedule busSchedule = (BusSchedule) session.get(BusSchedule.class, busScheduleID);
			transaction.commit();
			return busSchedule;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean updateBusSchedule(BusSchedule busSchedule) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			session.update(busSchedule);
			transaction.commit();
			return true;
		}catch(Exception e){
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

}
