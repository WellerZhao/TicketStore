package com.ticketstore.dao.impl;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.ticketstore.dao.IAdminDao;
import com.ticketstore.domain.Admin;
@Repository("adminDao")
public class AdminDao implements IAdminDao{
	private AdminDao adminDao = null;
	@Resource
	private SessionFactory sessionFactory = null;
	private AdminDao(){
		
	}
	
	public AdminDao getInstance(){
		if(adminDao == null){
			adminDao = new AdminDao();
		}
		return adminDao;
	}

	@Override
	public boolean login(Admin admin) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			if(!session.createQuery("from Admin as a where a.adminName=:aName").setParameter("aName", admin.getAdminName()).list().isEmpty()){
				if(!session.createQuery("from Admin as a where a.adminName=:aName and a.adminPassword=:aPwd")
						.setParameter("aName", admin.getAdminName()).setParameter("aPwd", admin.getAdminPassword()).list().isEmpty()){
					transaction.commit();
					return true;
				}
			}
			transaction.commit();
			return false;
		}catch (Exception e) {
			if(transaction != null){
				transaction.rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
	
	
}
