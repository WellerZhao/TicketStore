package com.ticketstore.dao;

import java.util.List;

import com.ticketstore.domain.Order;

public interface IOrderDao {
	public boolean insertOrder(Order order);
	public List<Order> listOrderForUser(Integer userID);
}
