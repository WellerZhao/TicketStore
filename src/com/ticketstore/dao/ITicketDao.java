package com.ticketstore.dao;

import java.util.List;

import com.ticketstore.domain.Ticket;

public interface ITicketDao {
	public List<Ticket> listAllTicket();
}
