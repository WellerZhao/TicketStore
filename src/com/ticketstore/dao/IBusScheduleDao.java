package com.ticketstore.dao;

import java.util.List;

import com.ticketstore.domain.BusSchedule;
import com.ticketstore.domain.Contractor;

public interface IBusScheduleDao {
	public List<BusSchedule> list();
	public List<BusSchedule> listForContractor(Contractor contractor);
	public boolean deleteBusSchedule(Integer busScheduleID);
	public boolean addBusSchedule(BusSchedule busSchedule);
	public List<BusSchedule> searchBusSchedule(String whereHQL, List<Object> params);
	public BusSchedule findBusSchedule(Integer busScheduleID);
	public boolean updateBusSchedule(BusSchedule busSchedule);
}
