package com.ticketstore.dao;

import com.ticketstore.domain.Admin;

public interface IAdminDao {
	public boolean login(Admin admin);
}
