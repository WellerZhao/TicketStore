package com.ticketstore.dao;

import java.util.List;

import com.ticketstore.domain.Contractor;

public interface IContractorDao {
	public List<Contractor> list();
	public Integer login(Contractor contractor);
	public boolean deleteContractor(Integer contractorID);
	public boolean addContractor(Contractor contractor);
	public Contractor findContractor(Integer contractorID);
	public boolean updateContractor(Contractor contractor);
}
