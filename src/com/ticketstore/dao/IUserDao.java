package com.ticketstore.dao;

import java.util.List;

import com.ticketstore.domain.User;

public interface IUserDao {
	public Integer login(User user);
	public boolean register(User user);
	public List<User> list();
	public boolean deleteUser(Integer userID);
	public boolean addUser(User user);
}
