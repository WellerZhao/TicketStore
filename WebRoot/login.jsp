<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <title>用户登录   - 售票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mycss.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="mid_center">
			<s:form cssClass="form-horizontal" method="post" action="/userAction/loginAction.action">
				<fieldset>
					<legend>登录</legend>
			
					<div class="control-group">
				
					<!-- Text input-->
					<label class="control-label" for="input01">账号</label>
					<div class="controls">
						<s:textfield name="user.userName" cssClass="input-middle" placeholder="账号"></s:textfield>
						<p class="help-block">例如：ticketstore@qq.com</p>
					</div>
					</div>
			
					<div class="control-group">
					<!-- Text input-->
					<label class="control-label" for="input01">密码</label>
						<div class="controls">
						<s:password name="user.password" cssClass="input-middle" placeholder="密码"></s:password>
						</div>
					</div>
			
					<div class="control-group">
						<label class="control-label"></label>
						<div class="controls">
							<!-- Multiple Checkboxes -->
							<label class="checkbox">
								<input type="checkbox" value="记住我？">
								记住我？
							</label>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label"></label>
						<!-- Button -->
						<div class="controls">
							<button class="btn btn-success" style="height:40px;width:217px;font-size:25px;font-weight: bold;" onclick="document.getElementById('login_form').submit()">登录</button>
						</div>
					</div>
				</fieldset>
			</s:form>
  		</div>
	</div>
  </body>
</html>

