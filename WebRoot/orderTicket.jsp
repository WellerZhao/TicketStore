<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>预订车票</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

  </head>
  
  <body>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row-fluid">
			<p align="center">订票</p>
			<label>您预订的车票</label><br/>
			<s:form action="/orderAction/orderTicketAction.action" method="post">
				<s:textfield name="busScheduleID" cssClass="input-small" readonly="true">班次ID：</s:textfield><br/>
				<br/>
				<label>起始地：${busSchedule.fromLocation }</label><br/>
				<label>目的地：${busSchedule.toLocation }</label><br/>
				<label>发车时间：${busSchedule.takeOffTime }</label><br/>
				<label>车票单价：${busSchedule.priceForASeat }</label><br/>
				<s:textfield name="order.orderTicketNum" cssClass="input-small">预订数量：</s:textfield><br/>
				<br/>
				<button type="submit" class="btn left_bank btn-success">生成订单</button>
			</s:form>
		</div>
	</div>
  </body>
</html>
