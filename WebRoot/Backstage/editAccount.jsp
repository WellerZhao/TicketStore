<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
   	<base href="<%=basePath%>">
    <title>编辑账户 - 购票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mycss.css">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<script language="JavaScript">
		function hao() {
   			if (document.form1.ok.value=="编辑")
  			{
  				document.getElementById("test").disabled=false;
     			document.form1.ok.value="保存";
   			}
   			else
   			{
     			document.form1.ok.value="编辑";
     			document.getElementById("test").disabled=true;
   			}
		}
	</script>


	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span15">
				<table class="table table-striped table-hover" contenteditable="false">
					<thead>
						<tr>
							<th>用户名</th>
							<th>真实姓名</th>
							<th>联系电话</th>
							<th>登录密码</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<s:iterator id="user" value="userList">
							<tr>
						<td><input type="text" id="test" style="background:transparent;border:0px solid #ffffff" disabled="disabled"  value="${user.userName }"/></td>
								<td><input type="text" id="test" style="background:transparent;border:0px solid #ffffff" disabled="disabled"  value="${user.realName }"/></td>
								<td><input type="text" id="test" style="background:transparent;border:0px solid #ffffff" disabled="disabled"  value="${user.telephoneNum }"/></td>
								<td><input type="text" id="test" style="background:transparent;border:0px solid #ffffff" disabled="disabled"  value="${user.password}"/></td>
								<td><input class="btn btn-success" type="button" id="ok" value="编辑" onclick="hao();"/></button>
								<a button class="btn btn-danger" type="button" onclick="return confirm('确认删除${user.userName }该用户吗？');"
								href="/TicketStore/userAction/deleteUserAction.action?userID=${user.userID}"
								target="showManager">删除</a></button></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </body>
</html>

