<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
   	<base href="<%=basePath%>">
    <title>添加用户 - 购票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="./css/mycss.css">

  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div class="mid_center">
				<s:form cssClass="form-horizontal" method="post" action="/userAction/addUserAction.action" target="showManager">
					<fieldset>
						<div id="legend" class="">
							<legend>添加用户</legend>
						</div>
						
						<div class="control-group">
				
						<!-- Text input-->
							 <label class="control-label" for="input01">用户账号</label>
							  <div class="controls">
							  	<s:textfield name="user.userName"  cssClass="input-middle" placeholder="用户账号"></s:textfield>
								<p class="help-block"></p>
							  </div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="input01">真实姓名</label>
							 <div class="controls">
								<s:textfield name="user.realName" cssClass="input-middle" placeholder="真实姓名"></s:textfield>
								<p class="help-block"></p>
							 </div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="input01">联系电话</label>
							  <div class="controls">
								<s:textfield name="user.telephoneNum" cssClass="input-middle" placeholder="联系电话"></s:textfield>
								<p class="help-block"></p>
							  </div>
						</div>
				
				
						<div class="control-group">
							<!-- Text input-->
							<label class="control-label" for="input01">登录密码</label>
							<div class="controls">
							    <s:password name="user.password" cssClass="input-middle" placeholder="登录密码"></s:password>
								<p class="help-block"></p>
							</div>
						</div>
				
						<div class="control-group">
							<!-- Text input-->
							<label class="control-label" for="input01">确认密码</label>
							<div class="controls">
								<s:password cssClass="input-middle" placeholder="确认密码"></s:password>
								<p class="help-block"></p>
							</div>
						</div>
				
						
				
						<div class="control-group">
							<label class="control-label"></label>
							<div class="controls">
							  <!-- Multiple Checkboxes -->
							  <label class="checkbox">
								<input value="我已经仔细阅读并接受 车票网注册条款" type="checkbox">
								我已经仔细阅读并接受车票网各条款
							  </label>
						    </div>
						</div>
				
						<div class="control-group">
							<label class="control-label"></label>
							<!-- Button -->
							<div class="controls">
								<button class="btn btn-success" style="height:40px;width:217px;font-size:25px;font-weight: bold;" type="submit">添加</button>
							</div>
						</div>
					</fieldset>
				</s:form>
			</div>
		</div>
		</div>
	</div>
  </body>
</html>

