<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
   	<base href="<%=basePath%>">
    <title>后台登陆 - 购票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mycss.css">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="bootstrap-responsive.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js" charset="UTF-8"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body >
    <div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="mid_center">
			
				<s:form id="admin_login_form" cssClass="form-horizontal" method="post" action="/adminAction/loginAction.action">
					<fieldset>
						<legend>后台登录</legend> 
						<div class="control-group">
							 <label class="control-label" for="inputEmail">用户名</label>
							<div class="controls">
								<s:textfield name="admin.adminName"></s:textfield>
							</div>
						</div>
						<div class="control-group">
							 <label class="control-label" for="inputPassword">密码</label>
							<div class="controls">
								<s:password name="admin.adminPassword"></s:password>
							</div>
						</div>
						  
						<div class="control-group">
							<div class="controls">
								 <label class="checkbox"><input type="checkbox" /> 记住我？</label> 
								 <button class="btn btn-success" style="height:40px;width:217px;font-size:25px;font-weight: bold;" type="submit" class="btn" onclick="document.getElementById('admin_login_form').submit()">登录</button>
							</div>
						</div>
					</fieldset> 
				</s:form>
				
			</div>
		</div>
	</div>
	</div>
  </body>
</html>

