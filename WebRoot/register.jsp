<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <title>用户注册 - 售票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mycss.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript">
    	function checkRegister(){
    		var flag = true;
    		var userName = document.getElementById("userName").value;
    		var realName = document.getElementById("realName").value;
    		var telNumber = document.getElementById("telNumber").value;
    		var pass1 = document.getElementById("pass1").value;
    		var pass2 = document.getElementById("pass2").value;
    		if (userName.length == 0){
    			document.getElementsByTagName('p').item(0).innerHTML = '用户名不能为空！！';
    			flag = false;
    		}
    		if (realName.length == 0){
    			document.getElementsByTagName('p').item(1).innerHTML = '真实姓名不能为空！！';
    			flag = false;
    		}
    		if (telNumber.length == 0){
    			document.getElementsByTagName('p').item(2).innerHTML = '联系电话不能为空！！';
    			flag = false;
    		}
    		if (pass1.length == 0){
    			document.getElementsByTagName('p').item(3).innerHTML = '密码不能为空！！';
    			flag = false;
    		}
    		if (pass2.length == 0){
    			document.getElementsByTagName('p').item(4).innerHTML = '密码不能为空！！';
    			flag = false;
    		}
    		if (pass1 != pass2){
    			document.getElementsByTagName('p').item(4).innerHTML = '两次输入密码不一致，请重新输入！！';
    			flag = false;
    		}
    		if (flag == true){
    			document.getElementById('register_form').submit();
    		}
    		return flag;
    	}
    </script>
  </head>
  <body>
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	
	<div class="container-fluid">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="mid_center">
		<s:form id="register_form" cssClass="form-horizontal" action="/userAction/registerAction.action">
			<fieldset>
		  		<div id="legend" class="">
					<legend class="">注册</legend>
		  		</div>
		
				<div class="control-group">

			  	<!-- Text input-->
					<label class="control-label" for="input01">用户名</label>
			  		<div class="controls">
			  			<s:textfield id="userName" placeholder="用户名" cssClass="input-middle" name="user.userName"></s:textfield>
						<p class="help-block tips" ></p>
			  		</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input01">真实姓名</label>
					<div class="controls">
			  			<s:textfield id="realName" placeholder="真实姓名" cssClass="input-middle" name="user.realName"></s:textfield>
						<p class="help-block tips"></p>
			  		</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input01">联系电话</label>
					<div class="controls">
			  			<s:textfield id="telNumber" placeholder="联系电话" cssClass="input-middle" name="user.telephoneNum"></s:textfield>
						<p class="help-block tips"></p>
			  		</div>
				</div>

				<div class="control-group">
				<!-- Text input-->
			  	<label class="control-label" for="input01">登录密码</label>
			  	<div class="controls">
			    	<s:password id="pass1" cssClass="input-middle" placeholder="登录密码" name="user.password"></s:password>
					<p class="help-block tips"></p>
			  	</div>
				</div>

				<div class="control-group">
			  	<!-- Text input-->
			  	<label class="control-label" for="input01">确认密码</label>
				<div class="controls">
			  		<s:password id="pass2" cssClass="input-middle" placeholder="确认密码" name="confirmPassword"></s:password>
					<p class="help-block tips"></p>
			  	</div>
				</div>
		

				<div class="control-group">
			  		<label class="control-label"></label>
			  		<div class="controls">
			 		 	<!-- Multiple Checkboxes -->
			  			<label class="checkbox">
						<input value="我已经仔细阅读并接受 车票网注册条款" type="checkbox"/>
						<p>我已经仔细阅读并接受</p>
						<p> 车票网注册条款</p>
		  				</label>
	  				</div>
				</div>

				<div class="control-group">
					<label class="control-label"></label>
				  	<!-- Button -->
				  	<div class="controls">
						<button class="btn btn-success" style="height:40px;width:217px;font-size:25px;font-weight: bold;"onclick="return checkRegister()">注册</button>
			 	 	</div>
				</div>
			</fieldset>
		</s:form>
		</div>
	</div>
  </body>
</html>

