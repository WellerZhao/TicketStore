<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>我的订单</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

  </head>
  
  <body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row-fluid">
		    <p align="center">您的订单</p>
		    <table class="table table-striped">
		    	<thead>
					<tr>
						<th>订单号</th>
						<th>出发地</th>
						<th>目的地</th>
						<th>出发时间</th>
						<th>票价</th>
						<th>订购票数</th>
						<th>订单总价</th>
						<th>下单日期</th>
						<th>已付款</th>
					</tr>
				</thead>
				<tbody>
			    	<s:iterator id="your_order" value="orderList">
			    		<tr>
			    			<td>${your_order.orderID }</td>
			    			<td>${your_order.busSchedule.fromLocation }</td>
			    			<td>${your_order.busSchedule.toLocation }</td>
			    			<td>${your_order.busSchedule.takeOffTime }</td>
			    			<td>${your_order.busSchedule.priceForASeat }</td>
			    			<td>${your_order.orderTicketNum }</td>
			    			<td>${your_order.totalPrice }</td>
			    			<td>${your_order.orderDate }</td>
			    			<td>${your_order.pay }</td>
			    		</tr>
			    	</s:iterator>
		    	</tbody>
		    </table>
		</div>
	</div>
  </body>
</html>
