<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <div class="row-fluid">
		<div class="span12">
			<div class="navbar">
				<div class="navbar-inner">
					<div class="container-fluid">
					<a class="btn btn-navbar" data-target=".navbar-responsive-collapse" data-toggle="collapse"> </a> 
					<a class="brand" href="index.jsp">售票网</a>
						<div class="nav-collapse collapse navbar-responsive-collapse">
							<ul class="nav">
								<li >
									<a href="index.jsp">首页</a>
								</li>
								<li>
									<a href="/TicketStore/bus_scheduleAction/listAllBusScheduleAction.action">车票预订</a>
								</li>
							</ul>
							<c:choose>
							<c:when test="${sessionScope.LoginedUser == null }">
								<ul class="nav pull-right">
									<li>
										<a href="login.jsp">登录</a>
									</li>
									<li>
										<a href="register.jsp">注册</a>
									</li>
								</ul>
							</c:when>
							<c:otherwise>
								<ul class="nav pull-right">
									<li>
										<a href="/TicketStore/orderAction/listOrderForUserAction.action">欢迎您：${sessionScope.LoginedUser.userName }</a>
									</li>
									<li>
										<a href="/TicketStore/userAction/logoutAction.action">注销</a>
									</li>
								</ul>							
							</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
  			</div>
  		</div>
	</div>

