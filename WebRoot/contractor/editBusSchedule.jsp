<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
   	<base href="<%=basePath%>">
    <title>管理车程班次 - 购票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span15">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>出发地</th>
							<th>目的地</th>
							<th>出发时间</th>
							<th>票价</th>
							<th>总票数</th>
							<th>剩余票数</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<s:iterator id="bus_schedule" value="busScheduleList">
							<tr>
								<td>${bus_schedule.fromLocation }</td>
								<td>${bus_schedule.toLocation }</td>
								<td>${bus_schedule.takeOffTime }</td>
								<td>${bus_schedule.priceForASeat }</td>
								<td>${bus_schedule.ticketNum }</td>
								<td>${bus_schedule.ticketLeftNum }</td>
								<td>
									<a class="btn btn-success" type="button" href="ticketInfo.jsp">编辑</a>
									<a class="btn btn-danger" type="button" 
									href="/TicketStore/bus_scheduleAction/deleteBusScheduleAction.action?busScheduleID=${bus_schedule.busScheduleID }"
									target="showManager">删除</a>
								</td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	
	<script type="text/javascript" src="jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
	<script type="text/javascript">
		$('#datetimepicker').datetimepicker('setStartDate', '2012-01-01');
		$('#datetimepicker').datetimepicker();
	</script>
  </body>
</html>

