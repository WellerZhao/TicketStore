<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
   	<base href="<%=basePath%>">
    <title>购票网 - 添加班次</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="./css/mycss.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div class="mid_center">
					<s:form cssClass="form-horizontal" method="post" action="/bus_scheduleAction/addBusScheduleAction.action" target="showManager">
						<fieldset>
							<legend>添加班次</legend> 
							<div class="control-group">
								<label class="control-label" for="input01">出发地</label>
							 	<div class="controls">
							  		<s:textfield name="busSchedule.fromLocation" cssClass="input-middle" placeholder="出发地"></s:textfield>
									<p class="help-block"></p>
							 	</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">目的地</label>
							 	<div class="controls">
							  		<s:textfield name="busSchedule.toLocation" cssClass="input-middle" placeholder="目的地"></s:textfield>
									<p class="help-block"></p>
							 	</div>
							</div>
							<div class="control-group">
							<label  class="control-label" for="input01">发车时间</label>
								<div class="controls">
									<div class="input-append date" id="datetimepicker" data-date-format="yyyy-mm-dd hh:ii:00">
										<input name="busSchedule.takeOffTime" size="100" type="text" placeholder="yyyy-mm-dd">
										<span class="add-on"><i class="icon-th"></i></span>
									</div>  
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">票价</label>
							 	<div class="controls">
							  		<s:textfield name="busSchedule.priceForASeat" cssClass="input-middle" placeholder="票价"></s:textfield>
									<p class="help-block"></p>
							 	</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">总票数</label>
							 	<div class="controls">
							  		<s:textfield name="busSchedule.ticketNum" cssClass="input-middle" placeholder="总票数"></s:textfield>
									<p class="help-block"></p>
							 	</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">剩余票数</label>
							 	<div class="controls">
							  		<s:textfield name="busSchedule.ticketLeftNum" cssClass="input-middle" placeholder="剩余票数"></s:textfield>
									<p class="help-block"></p>
							 	</div>
							</div>
							<div class="control-group">
								<div class="controls">
								<button class="btn btn-success" style="height:40px;width:217px;font-size:25px;font-weight: bold;" type="submit">提交</button>
								</div>
							</div>
						</fieldset>
					</s:form>
				</div>
			</div>
		</div>	
	</div>
	
	<script type="text/javascript" src="jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
	<script type="text/javascript">
		$('#datetimepicker').datetimepicker('setStartDate', '2012-01-01');
		$('#datetimepicker').datetimepicker();
	</script>
  </body>
</html>

