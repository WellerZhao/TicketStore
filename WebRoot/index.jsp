<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <title>首页 - 售票网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="tabbable" id="tabs-291660">
				<ul class="nav nav-tabs">
					<li class="active">
						<a data-toggle="tab" href="#panel-564786">最新动态</a>
					</li>
					<li>
						<a data-toggle="tab" href="#panel-188576">常见问题</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="panel-564786">
						<div class="accordion" id="accordion-805271">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-parent="#accordion-805271" data-toggle="collapse" href="#accordion-element-807389">关于暑假包车优惠有关事宜的公告</a>
								</div>
								<div class="accordion-body collapse" id="accordion-element-807389">
									<div class="accordion-inner">
										<span>为适应旅客需求，充分利用铁路运输能力，2014年3月20日—12月31日期间（9月30日~10月7日除外）始发的部分旅客列车末端空闲卧铺执行优惠票价，即：</span><br /> 
										<span>200公里内硬卧（上、中、下铺）优惠票价按照该次列车对应硬座票价的170%计算，软卧（上、下铺）优惠票价按照该次列车对应硬座票价的270%计算。</span><br /> 
										<span>200公里至400公里间硬卧（上、中、下铺）优惠票价按照该次列车对应硬座票价的158%计算，软卧（上、下铺）优惠票价按照该次列车对应硬座票价的258%计算。</span><br /> 
										<span>已享受半价卧铺票的旅客不再享受以上优惠。</span>
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-parent="#accordion-805271" data-toggle="collapse" href="#accordion-element-631041">包车购票身份核验须知</a>
								</div>
								<div class="accordion-body collapse" id="accordion-element-631041">
									<div class="accordion-inner">
										为了进一步完善包车实名制购票工作，自2014年3月1日起，本网站将对互联网注册用户和常用联系人（乘车人）进行身份信息核验。
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="panel-188576">
						<div class="accordion" id="accordion-860798">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-parent="#accordion-292453" data-toggle="collapse" href="#accordion-element-292453">身份核验结果有哪几种状态？</a>
								</div>
								<div class="collapse" id="accordion-element-292453">
									<div class="accordion-inner">
										对注册用户和常用联系人（乘车人）进行身份信息核验后，持二代居民身份证的有“已通过”“待核验”和“未通过”三种状态，持按规定可使用的有效护照、港澳居民来往内地通行证、台湾居民来往大陆通行证的有“已通过”“请报验”“预通过”和“未通过”四种状态。
									</div>
								</div>
							</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-parent="#accordion-860798" data-toggle="collapse" href="#accordion-element-177969">选<span>扣款成功但购票不成功怎么办？</span></a>
							</div>
							<div class="accordion-body collapse" id="accordion-element-177969">
								<div class="accordion-inner">
									这可能是由于支付时间过长导致的。遇到这一问题后，首先，请不要多次重复购票，以避免多次扣款；其次，请点击附件，下载表格，正确填写相关内容后，将表格以附件方式发送邮件至kyfw@12306.cn，我们将尽快核实后，按银行规定将所扣款项返还至您的银行卡。
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
  </body>
</html>

