/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50534
Source Host           : localhost:3306
Source Database       : ticketstore

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-06-03 10:28:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(20) NOT NULL,
  `adminPassword` varchar(20) NOT NULL,
  PRIMARY KEY (`adminID`),
  UNIQUE KEY `adminName` (`adminName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin');

-- ----------------------------
-- Table structure for bus_schedule
-- ----------------------------
DROP TABLE IF EXISTS `bus_schedule`;
CREATE TABLE `bus_schedule` (
  `busScheduleID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLocation` varchar(50) NOT NULL,
  `toLocation` varchar(50) NOT NULL,
  `takeOffTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priceForASeat` int(11) NOT NULL,
  `ticketNum` int(11) NOT NULL,
  `ticketLeftNum` int(11) NOT NULL,
  `contractorID` int(11) NOT NULL,
  PRIMARY KEY (`busScheduleID`),
  KEY `bus_contractorID` (`contractorID`),
  CONSTRAINT `bus_contractorID` FOREIGN KEY (`contractorID`) REFERENCES `contractor` (`contractorID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bus_schedule
-- ----------------------------
INSERT INTO `bus_schedule` VALUES ('28', '珠海北理工', '东莞常平', '2014-05-31 18:30:00', '50', '60', '60', '1');
INSERT INTO `bus_schedule` VALUES ('29', '珠海北理工', '广州岗顶', '2014-06-06 10:25:00', '35', '60', '44', '2');
INSERT INTO `bus_schedule` VALUES ('30', '珠海北理工', '深圳大学', '2014-06-04 12:50:00', '50', '60', '55', '2');
INSERT INTO `bus_schedule` VALUES ('31', '珠海北理工', '韶关', '2014-06-03 21:30:00', '150', '60', '60', '2');
INSERT INTO `bus_schedule` VALUES ('33', '广州岗顶', '珠海北师大', '2014-06-04 13:30:00', '35', '60', '0', '1');
INSERT INTO `bus_schedule` VALUES ('34', '深圳大学', '珠海北师大', '2014-06-04 17:30:00', '50', '60', '58', '2');
INSERT INTO `bus_schedule` VALUES ('35', '珠海北理工', '韶关', '2014-06-07 10:30:00', '150', '60', '60', '1');
INSERT INTO `bus_schedule` VALUES ('36', '广州岗顶', '珠海北理工', '2014-06-05 14:30:00', '35', '60', '60', '1');
INSERT INTO `bus_schedule` VALUES ('37', '深圳大学', '珠海北师大', '2014-06-06 10:50:00', '50', '70', '68', '1');
INSERT INTO `bus_schedule` VALUES ('38', '东莞常平', '珠海', '2014-06-10 15:55:00', '60', '65', '64', '1');

-- ----------------------------
-- Table structure for contractor
-- ----------------------------
DROP TABLE IF EXISTS `contractor`;
CREATE TABLE `contractor` (
  `contractorID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `realName` varchar(20) NOT NULL,
  `telephoneNum` varchar(11) NOT NULL,
  `totalProfit` int(11) DEFAULT NULL,
  `tradeMark` varchar(20) NOT NULL,
  PRIMARY KEY (`contractorID`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contractor
-- ----------------------------
INSERT INTO `contractor` VALUES ('1', 'laizuling', '653204', '赖祖灵', '13058579780', '2260', '恐龙翔');
INSERT INTO `contractor` VALUES ('2', 'yuzebin', '123456', '余泽斌', '13058555777', '910', '肥家鸟');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `newsID` int(11) NOT NULL,
  `newsTitle` varchar(100) DEFAULT NULL,
  `newsContent` varchar(255) DEFAULT NULL,
  `newsDate` datetime DEFAULT NULL,
  PRIMARY KEY (`newsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `noticeID` int(11) NOT NULL,
  `noticeTitle` varchar(100) DEFAULT NULL,
  `noticeContent` varchar(255) DEFAULT NULL,
  `noticeDate` datetime DEFAULT NULL,
  PRIMARY KEY (`noticeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notice
-- ----------------------------

-- ----------------------------
-- Table structure for orderlist
-- ----------------------------
DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `busScheduleID` int(11) NOT NULL,
  `orderTicketNum` int(11) NOT NULL,
  `totalPrice` int(11) NOT NULL,
  `orderDate` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `isPay` binary(1) NOT NULL,
  PRIMARY KEY (`orderID`),
  KEY `order_userID` (`userID`),
  KEY `order_busScheduleID` (`busScheduleID`),
  CONSTRAINT `order_busScheduleID` FOREIGN KEY (`busScheduleID`) REFERENCES `bus_schedule` (`busScheduleID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderlist
-- ----------------------------
INSERT INTO `orderlist` VALUES ('15', '34', '2', '100', '2014-06-02 20:55:06', '1', 0x30);
INSERT INTO `orderlist` VALUES ('16', '33', '60', '2100', '2014-06-02 20:56:29', '1', 0x30);
INSERT INTO `orderlist` VALUES ('31', '29', '1', '35', '2014-06-03 10:25:14', '1', 0x30);
INSERT INTO `orderlist` VALUES ('32', '37', '2', '100', '2014-06-03 10:27:46', '37', 0x30);

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `ticketID` int(11) NOT NULL AUTO_INCREMENT,
  `busScheduleID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`ticketID`),
  KEY `busScheduleID` (`busScheduleID`),
  KEY `ticket_userID` (`userID`),
  CONSTRAINT `busScheduleID` FOREIGN KEY (`busScheduleID`) REFERENCES `bus_schedule` (`busScheduleID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ticket_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ticket
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `realName` varchar(10) NOT NULL,
  `telephoneNum` varchar(11) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userName` (`userName`),
  UNIQUE KEY `telephoneNum` (`telephoneNum`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'laizuling', '653204', '赖祖灵', '13058579780');
INSERT INTO `user` VALUES ('37', 'yzb', '123', '123', '12354655558');
